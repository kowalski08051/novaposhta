<?php

$installer = $this;

$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('goodahead_novaposhta/area'))
    ->addColumn('area_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
      ), 'Id')
    ->addColumn('area_name', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
      ), 'Name')
    ->addColumn('area_ref', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
      ), 'Referal')
    ->addColumn('center_ref', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
      ), 'Center referal');
$installer->getConnection()->createTable($table);

$table = $installer->getConnection()
    ->newTable($installer->getTable('goodahead_novaposhta/city'))
    ->addColumn('city_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
      ), 'Id')
    ->addColumn('city_name_ua', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
      ), 'Name_ua')
    ->addColumn('city_name_ru', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
      ), 'Name_ru')
    ->addColumn('city_ref', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
      ), 'Referal')
    ->addColumn('delivery_mon', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
        'nullable'  => false,
      ), 'Delivery on Monday')
    ->addColumn('delivery_tue', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
        'nullable'  => false,
      ), 'Delivery on Tuesday')
    ->addColumn('delivery_wed', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
        'nullable'  => false,
      ), 'Delivery on Wednesday')
    ->addColumn('delivery_thu', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
        'nullable'  => false,
      ), 'Delivery on Thursday')
    ->addColumn('delivery_fri', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
        'nullable'  => false,
      ), 'Delivery on Friday')
    ->addColumn('delivery_sat', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
        'nullable'  => false,
      ), 'Delivery on Saturday')
    ->addColumn('delivery_sun', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
        'nullable'  => false,
      ), 'Delivery on Sunday')
    ->addColumn('area_ref', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
      ), 'Area referal');

$installer->getConnection()->createTable($table);

$table = $installer->getConnection()
    ->newTable($installer->getTable('goodahead_novaposhta/warehouse'))
    ->addColumn('warehouse_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
      ), 'Id')
    ->addColumn('number', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
      ), 'Warehouse number')
    ->addColumn('warehouse_name_ua', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
      ), 'Name_ua')
    ->addColumn('warehouse_name_ru', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
      ), 'Name_ru')
    ->addColumn('phone', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
      ), 'Phone')
    ->addColumn('warehouse_ref', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
      ), 'Referal')
    ->addColumn('city_ref', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
      ), 'City referal')
    ->addColumn('max_weight', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(), 'Max weight allowed');

$installer->getConnection()->createTable($table);

$installer->endSetup();
