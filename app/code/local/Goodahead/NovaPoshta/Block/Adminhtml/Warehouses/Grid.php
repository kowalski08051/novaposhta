<?php

class Goodahead_NovaPoshta_Block_Adminhtml_Warehouses_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
         parent::__construct();
         $this->setDefaultSort('city_ref');
         $this->setId('warehouses_grid');
         $this->setDefaultDir('asc');
         $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
         /** @var $collection Goodahead_NovaPoshta_Model_Resource_Warehouse_Collection */
         $collection = Mage::getModel('goodahead_novaposhta/warehouse')
                 ->getCollection();
         $collection->getSelect()->join(
                  array('city'=>Mage::getSingleton('core/resource')->getTableName('goodahead_novaposhta_city')),
                  'main_table.city_ref = city.city_ref',
                  array('city.city_name_ua'));
         $this->setCollection($collection);
         return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
    $this->addColumn('warehouse_id',
      array(
        'header' => $this->__('ID'),
        'align' =>'right',
        'width' => '50px',
        'index' => 'warehouse_id'
    ));

    $this->addColumn('name_ua',
      array(
        'header' => $this->__('Address (ua)'),
        'index' => 'warehouse_name_ua'
    ));

    $this->addColumn('city_name_ua',
      array(
        'header' => $this->__('City'),
        'index' => 'city_name_ua'
    ));

    $this->addColumn('phone',
      array(
        'header' => $this->__('Phone'),
        'index' => 'phone'
      ));

    $this->addColumn('max_weight',
      array(
        'header' => $this->__('Max weight'),
        'index' => 'max_weight'
    ));
    return parent::_prepareColumns();
  }

  public function getRowUrl($row)
  {
    return false;
  }
}
