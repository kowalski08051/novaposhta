<?php

class Goodahead_NovaPoshta_Block_Onepage_Shipping_Method_Additional extends Mage_Checkout_Block_Onepage_Shipping_Method_Additional
{
  /**
   * @return Goodahead_NovaPoshta_Model_Warehouse|bool
   */
  public function getWarehouse()
  {
      /** @var Mage_Sales_Model_Quote $quote */
      $quote = Mage::getSingleton('checkout/session')->getQuote();
      $warehouseId = $quote->getShippingAddress()->getData('warehouse_id');
      if ($warehouseId) {
          $warehouse = Mage::getModel('goodahead_novaposhta/warehouse')->load($warehouseId);
          if ($warehouse->getId()) {
              return $warehouse;
          }
      }

      return false;
  }

  /**
   * @return bool|int
   */
  public function getCityId()
  {
      $cityId = (int) $this->getData('city_id');
      if (!$cityId) {
          $warehouse = $this->getWarehouse();
          if ($warehouse) {
              $cityId =  $warehouse->getCity()->getId();;
              $this->setData('city_id', $cityId);
          }
      }

      if ($cityId) {
          return $cityId;
      }

      return false;
  }

  /**
   * @return Goodahead_NovaPoshta_Model_Resource_City_Collection
   */
  public function getCities()
  {
      $collection = Mage::getResourceModel('goodahead_novaposhta/city_collection');
      $collection->setOrder('city_name_ua');

      return $collection;
  }

  /**
   * @return Goodahead_NovaPoshta_Model_Resource_Warehouse_Collection|bool
   */
  public function getWarehouses()
  {
      if ($cityId = $this->getCityId()) {
          /** @var Goodahead_NovaPoshta_Model_Resource_Warehouse_Collection $collection */
          $collection = Mage::getResourceModel('goodahead_novaposhta/warehouse_collection');
          $collection->addFieldToFilter('city_id', $cityId);
          $collection->setOrder('warehouse_name_ua');

          return $collection;
      }

      return false;
  }
}
