<?php

class Goodahead_NovaPoshta_Model_Import
{
  /** @var  array */
  protected $_existingAreas;
  /** @var  array */
  protected $_existingCities;
  /** @var  array */
  protected $_existingWarehouses;

  protected $_dataMapArea = array(
    'Description' => 'area_name',
    'Ref'         => 'area_ref',
    'AreasCenter' => 'center_ref'
  );
  protected $_dataMapCity = array(
    'CityID'        => 'city_id',
    'DescriptionRu' => 'city_name_ru',
    'Description'   => 'city_name_ua',
    'Ref'           => 'city_ref',
    'Delivery1'     => 'delivery_mon',
    'Delivery2'     => 'delivery_tue',
    'Delivery3'     => 'delivery_wed',
    'Delivery4'     => 'delivery_thu',
    'Delivery5'     => 'delivery_fri',
    'Delivery6'     => 'delivery_sat',
    'Delivery7'     => 'delivery_sun',
    'Area'          => 'area_ref'
  );
  protected $_dataMapWarehouse = array(
    'SiteKey'               => 'warehouse_id',
    'CityRef'               => 'city_ref',
    'Description'           => 'warehouse_name_ua',
    'DescriptionRu'         => 'warehouse_name_ru',
    'Phone'                 => 'phone',
    'TotalMaxWeightAllowed' => 'max_weight',
    'Ref'                   => 'warehouse_ref',
    'Number'                => 'number'
  );
  /**
   * @throws Exception
   * @return Goodahead_NovaPoshta_Model_Import
   */
  public function run()
  {
    $apiKey = Mage::helper('goodahead_novaposhta')->getStoreConfig('api_key');
    $apiUrl = Mage::helper('goodahead_novaposhta')->getStoreConfig('api_url');
    if (!$apiKey || !$apiUrl) {
      Mage::helper('goodahead_novaposhta')->log('No API key or API URL configured');
      throw new Exception('No API key or API URL configured');
    }
    try {
      /** @var $apiClient Goodahead_NovaPoshta_Model_Api_Client */
      $apiClient = Mage::getModel('goodahead_novaposhta/api_client', $apiKey);

      Mage::helper('goodahead_novaposhta')->log('Start area import');
      $areas = $apiClient->getAreas();
      $this->_importAreas($areas);
      Mage::helper('goodahead_novaposhta')->log('End area import');

      Mage::helper('goodahead_novaposhta')->log('Start city import');
      $cities = $apiClient->getCities();
      $this->_importCities($cities);
      Mage::helper('goodahead_novaposhta')->log('End city import');

      Mage::helper('goodahead_novaposhta')->log('Start warehouse import');
      $warehouses = $apiClient->getWarehouses();
      $this->_importWarehouses($warehouses);
      Mage::helper('goodahead_novaposhta')->log('End warehouse import');

    } catch (Exception $e) {
      Mage::logException($e);
      Mage::helper('goodahead_novaposhta')->log("Exception: \n" . $e->getMessage());
      throw $e;
    }
    return $this;
  }

  /**
   * @param array $areas
   * @return bool
   * @throws Exception
   */
  protected function _importAreas(array $areas)
  {
    if (empty($areas)) {
      Mage::helper('goodahead_novaposhta')->log('No areas received');
      throw new Exception('No areas received');
    }
    //die(var_dump($areas));
    $tableName  = Mage::getSingleton('core/resource')->getTableName('goodahead_novaposhta_area');
    $connection = $this->_getConnection();
    $areas = $this->_applyMap($areas, $this->_dataMapArea);
    $existingAreas = $this->_getExistingAreas();
    $areasToDelete = array_diff(array_keys($existingAreas), array_keys($areas));
    if (count($areasToDelete) > 0) {
      $connection->delete($tableName, $areasToDelete);
      Mage::helper('goodahead_novaposhta')->log(sprintf("Warehouses deleted: %s", implode(',', $areasToDelete)));
    }
    if (count($areas) > 0) {
      $tableName  = Mage::getSingleton('core/resource')->getTableName('goodahead_novaposhta_area');
      $connection = $this->_getConnection();
      $connection->beginTransaction();
      try {
        foreach ($areas as $data) {
          $connection->insertOnDuplicate($tableName, $data);
        }
        $connection->commit();
      } catch (Exception $e) {
        $connection->rollBack();
        throw $e;
      }
    }
    return true;
  }

  /**
   * @return array
   */
  protected function _getExistingAreas()
  {
    if (!$this->_existingAreas) {
      /** @var Goodahead_NovaPoshta_Model_Resource_City_Collection $collection */
      $collection = Mage::getResourceModel('goodahead_novaposhta/area_collection');
      $this->_existingAreas = $collection->getAllIds();
    }
    return $this->_existingAreas;
  }

  /**
   * @param array $cities
   * @return bool
   * @throws Exception
   */
  protected function _importCities(array $cities)
  {
    if (empty($cities)) {
      Mage::helper('goodahead_novaposhta')->log('No city received');
      throw new Exception('No city received');
    }
    $tableName  = Mage::getSingleton('core/resource')->getTableName('goodahead_novaposhta_city');
    $connection = $this->_getConnection();
    $cities = $this->_applyMap($cities, $this->_dataMapCity);
    $existingCities = $this->_getExistingCities();
    $citiesToDelete = array_diff(array_keys($existingCities), array_keys($cities));
    if (count($citiesToDelete) > 0) {
      $connection->delete($tableName, $citiesToDelete);
      Mage::helper('goodahead_novaposhta')->log(sprintf("Warehouses deleted: %s", implode(',', $citiesToDelete)));
    }
    if (count($cities) > 0) {
      $tableName  = Mage::getSingleton('core/resource')->getTableName('goodahead_novaposhta_city');
      $connection = $this->_getConnection();
      $connection->beginTransaction();
      try {
        foreach ($cities as $data) {
          $connection->insertOnDuplicate($tableName, $data);
        }
        $connection->commit();
      } catch (Exception $e) {
        $connection->rollBack();
        throw $e;
      }
    }
    return true;
  }
  /**
   * @return array
   */
  protected function _getExistingCities()
  {
    if (!$this->_existingCities) {
      /** @var Goodahead_NovaPoshta_Model_Resource_City_Collection $collection */
      $collection = Mage::getResourceModel('goodahead_novaposhta/city_collection');
      $this->_existingCities = $collection->getAllIds();
    }
    return $this->_existingCities;
  }

  /**
   * @param array $apiObjects
   * @param array $map
   * @return array
   */
  protected function _applyMap(array $apiObjects, array $map)
  {
    $newObjects = array();
    $i = 0;
    foreach ($apiObjects as $or => $obj) {
      foreach ($map as $api => $db) {
        $newObjects[$i][$db] = $obj[$api];
      }
      $i++;
    }
    return $newObjects;
  }
  /**
   * @return Varien_Db_Adapter_Interface
   */
  protected function _getConnection()
  {
    return Mage::getSingleton('core/resource')->getConnection('core_write');
  }
  /**
   * @param array $warehouses
   * @return bool
   * @throws Exception
   */
  protected function _importWarehouses(array $warehouses)
  {
    if (empty($warehouses)) {
      Mage::helper('goodahead_novaposhta')->log('No warehouses received');
      throw new Exception('No warehouses received');
    }
    $warehouses = $this->_applyMap($warehouses, $this->_dataMapWarehouse);
    $existingWarehouses = $this->_getExistingWarehouses();
    $warehousesToDelete = array_diff(array_keys($existingWarehouses), array_keys($warehouses));
    $tableName  = Mage::getSingleton('core/resource')->getTableName('goodahead_novaposhta_warehouse');
    $connection = $this->_getConnection();
    if (count($warehousesToDelete) > 0) {
      $connection->delete($tableName, $warehousesToDelete);
      Mage::helper('goodahead_novaposhta')->log(sprintf("Warehouses deleted: %s", implode(',', $warehousesToDelete)));
    }
    $connection->beginTransaction();
    try {
      foreach ($warehouses as $data) {
        $connection->insertOnDuplicate($tableName, $data);
      }
      $connection->commit();
    } catch (Exception $e) {
      $connection->rollBack();
      throw $e;
    }
    return true;
  }
  /**
   * @return array
   */
  protected function _getExistingWarehouses()
  {
    if (!$this->_existingWarehouses) {
      /** @var Goodahead_NovaPoshta_Model_Resource_Warehouse_Collection $collection */
      $collection = Mage::getResourceModel('goodahead_novaposhta/warehouse_collection');
      $this->_existingWarehouses = $collection->getAllIds();
    }
    return $this->_existingWarehouses;
  }
}
