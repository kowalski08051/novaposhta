<?php

class Goodahead_NovaPoshta_Model_Warehouse extends Mage_Core_Model_Abstract
{
  public function _construct()
  {
    $this->_init('goodahead_novaposhta/warehouse');
  }

  /**
    * @return Goodahead_NovaPoshta_Model_City
    */
  public function getCity()
  {
    return Mage::getModel('goodahead_novaposhta/city')->load($this->getData('city_ref'));
  }
}
