<?php

class Goodahead_NovaPoshta_Model_Observer
{
    public function insertBlock($observer)
    {
        /** @var $_block Mage_Core_Block_Abstract */
        /*Get block instance*/
      //S  die("bjvcjhgv");
        $_block = $observer->getBlock();
        /*get Block type*/
        $_type = $_block->getType();
        if ($_type == 'checkout/onepage_shipping_method_additional')
        {
            $_block->setTemplate('goodahead/novaposhta/additional_shipping.phtml');
        }
    }

    public function logCompiledLayout($o)
    {
        $req  = Mage::app()->getRequest();
        $info = sprintf(
            "\nRequest: %s\nFull Action Name: %s_%s_%s\nHandles:\n\t%s\nUpdate XML:\n%s",
            $req->getRouteName(),
            $req->getRequestedRouteName(),      //full action name 1/3
            $req->getRequestedControllerName(), //full action name 2/3
            $req->getRequestedActionName(),     //full action name 3/3
            implode("\n\t",$o->getLayout()->getUpdate()->getHandles()),
            $o->getLayout()->getUpdate()->asString()
        );

        // Force logging to var/log/layout.log
        Mage::log($info, Zend_Log::INFO, 'layout.log', true);
    }
}
