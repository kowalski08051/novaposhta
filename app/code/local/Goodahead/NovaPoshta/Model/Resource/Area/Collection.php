<?php

class Goodahead_NovaPoshta_Model_Resource_Area_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
  public function _construct()
  {
    $this->_init('goodahead_novaposhta/area');
  }
}
