<?php

class Goodahead_NovaPoshta_Model_Resource_City extends Mage_Core_Model_Resource_Db_Abstract
{
  public function _construct()
  {
    $this->_init('goodahead_novaposhta/city', 'city_id');
  }
}
