<?php

class Goodahead_NovaPoshta_Model_Resource_Warehouse extends Mage_Core_Model_Resource_Db_Abstract
{
  public function _construct()
  {
    $this->_init('goodahead_novaposhta/warehouse', 'warehouse_id');
  }
}
