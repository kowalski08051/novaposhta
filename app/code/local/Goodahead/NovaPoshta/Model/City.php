<?php

class Goodahead_NovaPoshta_Model_City extends Mage_Core_Model_Abstract
{
  public function _construct()
  {
    $this->_init('goodahead_novaposhta/city');
  }
}
