<?php
class Goodahead_NovaPoshta_Helper_Data extends Mage_Core_Helper_Data
{
  protected $_logFile = 'goodahead_novaposhta.log';

  /**
  * @param $string
  *
  * @return Goodahead_NovaPoshta_Helper_Data
  */
  public function log($string)
  {
    if ($this->getStoreConfig('enable_log')) {
      Mage::log($string, null, $this->_logFile);
    }
    return $this;
  }

  /**
  * @param string $key
  * @param null $storeId
  *
  * @return mixed
  */
  public function getStoreConfig($key, $storeId = null)
  {
    return Mage::getStoreConfig("carriers/goodahead_novaposhta/$key", $storeId);
  }
}
