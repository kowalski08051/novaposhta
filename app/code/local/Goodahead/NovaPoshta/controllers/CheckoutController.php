<?php
class Goodahead_NovaPoshta_CheckoutController
    extends Mage_Core_Controller_Front_Action
{
    /**
     * Render form for choose city and warehouse
     */
    public function formAction()
    {
    //  $this->loadLayout();
    //  $layout = $this->getLayout();
    //  $block = $layout->getBlock('goodahead.shipping.select');
      $block = $this->getLayout()->createBlock('goodahead_novaposhta/onepage_shipping_method_additional')
        ->setTemplate('goodahead/novaposhta/shipping_select.phtml');

      $this->getResponse()->setBody($block->toHtml());
    }

    public function cityAction()
    {
      $block = $this->getLayout()->createBlock('goodahead_novaposhta/onepage_shipping_method_additional')
      ->setTemplate('goodahead/novaposhta/shipping_select.phtml');

      if ($cityId = $this->getRequest()->getParam('city')) {
        $block->setData('city_id', $cityId);
      }
      $this->getResponse()->setBody($block->toHtml());
    }
}
