<?php

class Goodahead_NovaPoshta_WarehousesController extends Mage_Adminhtml_Controller_Action
{

  public function indexAction()
  {
    $this->_title($this->__('Sales'))->_title($this->__('Nova Poshta Warehouses'));
    $this->_initAction()
      ->_addContent($this->getLayout()->createBlock('goodahead_novaposhta/adminhtml_warehouses'))
      ->renderLayout();

    return $this;
  }

  public function gridAction()
    {
      $this->loadLayout();
      $this->getResponse()->setBody(
        $this->getLayout()->createBlock('goodahead_novaposhta/adminhtml_warehouses_grid')->toHtml()
      );
    }

  public function synchronizeAction()
  {
    try {
      Mage::getModel('goodahead_novaposhta/import')->run();
      $this->_getSession()->addSuccess($this->__('API synchronization finished'));
    }
    catch (Exception $e) {
      $this->_getSession()->addError($this->__('Error during synchronization: %s', $e->getMessage()));
    }

    $this->_redirect('*/*/index');
    return $this;
  }

  /**
  * Initialize action
  *
  * @return Ak_NovaPoshta_WarehousesController
  */
  protected function _initAction()
  {
    $this->loadLayout()
      ->_setActiveMenu('sales/goodahead_novaposhta/warehouses')
      ->_addBreadcrumb($this->__('Sales'), $this->__('Sales'))
      ->_addBreadcrumb($this->__('Nova Poshta Warehouses'), $this->__('Nova Poshta Warehouses'));
    return $this;
  }
}
